package com.hk.fl.serviceI;

import java.io.InputStream;
import java.util.List;

import com.hk.fl.domain.TempTable;
import com.hk.fl.domain.User;

/**
 * 
 * @author Vimal Bhaskar 02/19/2020
 *
 */
public interface ServiceI {
	
	public User authUser(User user) throws Exception;
	
	public List<TempTable> uploadFile(InputStream file) throws Exception;
	
	public List<TempTable> getData() throws Exception;

}
