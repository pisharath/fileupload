package com.hk.fl.serviceI.serviceImpl;

import java.io.InputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hk.fl.daoI.DaoI;
import com.hk.fl.domain.TempTable;
import com.hk.fl.domain.User;
import com.hk.fl.serviceI.ServiceI;

/**
 * 
 * @author Harish Kalepu 02/19/2020
 *
 */
@Service
@Transactional
public class ServiceImpl implements ServiceI {

	@Autowired
	private DaoI daoI;

	@Override
	public User authUser(User user) throws Exception {
		return daoI.authUser(user);
	}

	@Override
	public List<TempTable> uploadFile(InputStream file) throws Exception {
		return daoI.uploadFile(file);
	}

	@Override
	public List<TempTable> getData() throws Exception {
		return daoI.getData();
	}

}
