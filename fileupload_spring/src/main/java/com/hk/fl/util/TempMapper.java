package com.hk.fl.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hk.fl.domain.TempTable;

public class TempMapper implements RowMapper {

	@Override
	public TempTable mapRow(ResultSet rs, int rowNum) throws SQLException {
		TempTable tempTable = new TempTable();
		tempTable.setId(rs.getString("id"));
		tempTable.setSSN(rs.getString("name"));
		tempTable.setSYS_WD_OPT_CD(rs.getString("address"));
		tempTable.setReq_Amt(rs.getDouble("Req_Amt"));
		tempTable.setFREQ_CD(rs.getString("FREQ_CD"));
		tempTable.setSTART_DT(rs.getDate("START_DT"));

		tempTable.setSTOP_DT(rs.getDate("STOP_DT"));		
		tempTable.setPREV_PAYMENT_DT(rs.getDate("PREV_PAYMENT_DT"));
		tempTable.setNEXT_PAYMENT_DT(rs.getDate("NEXT_PAYMENT_DT"));
		tempTable.setFED_TAX_PCT(rs.getDouble("FED_TAX_PCT"));
		tempTable.setSTATE_TAX_PCT(rs.getDouble("STATE_TAX_PCT"));
		tempTable.setLOCAL_TAX_PCT(rs.getDouble("LOCAL_TAX_PCT"));
		
		tempTable.setEFT_ACCT_NO(rs.getString("EFT_ACCT_NO"));
		tempTable.setEFT_ACCT_TYP_C(rs.getString("EFT_ACCT_TYP_C"));
		tempTable.setABA_ROUTING_NO(rs.getString("ABA_ROUTING_NO"));
		tempTable.setDIRECT_DEPOSIT_C(rs.getString("DIRECT_DEPOSIT_C"));
	return tempTable;
	}

}

