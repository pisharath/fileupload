package com.hk.fl.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hk.fl.domain.TempTable;
import com.hk.fl.domain.User;
import com.hk.fl.serviceI.ServiceI;

/**
 * 
 * @author Vimal Bhaskar 11/28/2018
 *
 */
@RestController
@RequestMapping("/api")
public class Controller {

	private Logger logger = LoggerFactory.getLogger(Controller.class);

	private Map<String, Object> response = new HashMap<String, Object>();

	@Autowired
	private ServiceI serviceI;

	@PostMapping(value = "/user/auth")
	private Map<String, Object> authUser(@RequestBody User user) {
		logger.debug("***** Start authUser Method");
		response.clear();
		try {
			User result = serviceI.authUser(user);
			response.put("data", result);
			response.put("status", 200);
			logger.debug("***** End authUser Method");
		} catch (Exception ex) {
			logger.debug("***** Exception While authUser: " + ex.getLocalizedMessage());
			response.put("status", 400);
			response.put("message", "Error While authUser.");
			return response;
		}
		return response;
	}

	@PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public Map<String, Object> singleFileUpload(@RequestParam("file") MultipartFile file) {
		logger.debug("***** Start singleFileUpload Method");
		response.clear();
		if (file.isEmpty()) {
			response.put("message", "Please select a file to upload");
			response.put("status", "300");
			return response;
		}
		try {
			List<TempTable> result = serviceI.uploadFile(file.getInputStream());
			response.put("data", result);
			response.put("status", 200);
			logger.debug("***** End singleFileUpload Method");
		} catch (Exception ex) {
			logger.debug("***** Exception While Upload File: " + ex.getLocalizedMessage());
			response.put("status", 400);
			response.put("message", ex.getLocalizedMessage());
			return response;
		} finally {
		}
		return response;
	}

	@GetMapping(value = "/data")
	private Map<String, Object> getData() {
		logger.debug("***** Start getData Method");
		response.clear();
		List<TempTable> list;
		try {
			list = serviceI.getData();
			if (list != null) {
				response.put("status", 200);
				response.put("message", "Success");
				response.put("data", list);
			} else {
				response.put("status", 200);
				response.put("message", "No Records Found.");
			}
			logger.debug("***** End getData Method");
		} catch (Exception e) {
			logger.debug("***** Exception While getData: " + e.getLocalizedMessage());
			response.put("status", 400);
			response.put("message", "Error While getting Records.");
			return response;
		}
		return response;
	}

}
