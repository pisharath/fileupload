package com.hk.fl.domain;

import java.util.Date;

public class TempTable {

	private String id;
	private String SSN;
	private String SYS_WD_OPT_CD;
	private double Req_Amt; 
	private String FREQ_CD;
	private Date START_DT;
	private Date STOP_DT ; 
	private Date PREV_PAYMENT_DT ; 
	private Date NEXT_PAYMENT_DT ; 
	private double FED_TAX_PCT ; 
	private double STATE_TAX_PCT ; 
	private double LOCAL_TAX_PCT ; 
	private String EFT_ACCT_NO ; 
	private String EFT_ACCT_TYP_C ; 
	private String ABA_ROUTING_NO ; 
	private String DIRECT_DEPOSIT_C ; 
	
	
	public TempTable() {

	}

	public TempTable(String id, String SSN, String SYS_WD_OPT_CD,double Req_Amt ) {
		super();
		this.id = id;
		this.SSN = SSN;
		this.SYS_WD_OPT_CD = SYS_WD_OPT_CD;
		this.Req_Amt = Req_Amt;
	}

	public double getReq_Amt() {
		return Req_Amt;
	}

	public void setReq_Amt(double req_Amt) {
		Req_Amt = req_Amt;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSSN() {
		return SSN;
	}

	public void setSSN(String SSN) {
		this.SSN = SSN;
	}

	public String getSYS_WD_OPT_CD() {
		return SYS_WD_OPT_CD;
	}

	public void setSYS_WD_OPT_CD(String SYS_WD_OPT_CD) {
		this.SYS_WD_OPT_CD = SYS_WD_OPT_CD;
	}

	
	public String getFREQ_CD() {
		return FREQ_CD;
	}

	public void setFREQ_CD(String fREQ_CD) {
		FREQ_CD = fREQ_CD;
	}

	public Date getSTART_DT() {
		return START_DT;
	}

	public void setSTART_DT(Date sTART_DT) {
		START_DT = sTART_DT;
	}

	public Date getSTOP_DT() {
		return STOP_DT;
	}

	public void setSTOP_DT(Date sTOP_DT) {
		STOP_DT = sTOP_DT;
	}

	public Date getPREV_PAYMENT_DT() {
		return PREV_PAYMENT_DT;
	}

	public void setPREV_PAYMENT_DT(Date pREV_PAYMENT_DT) {
		PREV_PAYMENT_DT = pREV_PAYMENT_DT;
	}

	public Date getNEXT_PAYMENT_DT() {
		return NEXT_PAYMENT_DT;
	}

	public void setNEXT_PAYMENT_DT(Date nEXT_PAYMENT_DT) {
		NEXT_PAYMENT_DT = nEXT_PAYMENT_DT;
	}

	public double getFED_TAX_PCT() {
		return FED_TAX_PCT;
	}

	public void setFED_TAX_PCT(double fED_TAX_PCT) {
		FED_TAX_PCT = fED_TAX_PCT;
	}

	public double getSTATE_TAX_PCT() {
		return STATE_TAX_PCT;
	}

	public void setSTATE_TAX_PCT(double sTATE_TAX_PCT) {
		STATE_TAX_PCT = sTATE_TAX_PCT;
	}

	public double getLOCAL_TAX_PCT() {
		return LOCAL_TAX_PCT;
	}

	public void setLOCAL_TAX_PCT(double lOCAL_TAX_PCT) {
		LOCAL_TAX_PCT = lOCAL_TAX_PCT;
	}

	public String getEFT_ACCT_NO() {
		return EFT_ACCT_NO;
	}

	public void setEFT_ACCT_NO(String eFT_ACCT_NO) {
		EFT_ACCT_NO = eFT_ACCT_NO;
	}

	public String getEFT_ACCT_TYP_C() {
		return EFT_ACCT_TYP_C;
	}

	public void setEFT_ACCT_TYP_C(String eFT_ACCT_TYP_C) {
		EFT_ACCT_TYP_C = eFT_ACCT_TYP_C;
	}

	public String getABA_ROUTING_NO() {
		return ABA_ROUTING_NO;
	}

	public void setABA_ROUTING_NO(String aBA_ROUTING_NO) {
		ABA_ROUTING_NO = aBA_ROUTING_NO;
	}

	public String getDIRECT_DEPOSIT_C() {
		return DIRECT_DEPOSIT_C;
	}

	public void setDIRECT_DEPOSIT_C(String dIRECT_DEPOSIT_C) {
		DIRECT_DEPOSIT_C = dIRECT_DEPOSIT_C;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
