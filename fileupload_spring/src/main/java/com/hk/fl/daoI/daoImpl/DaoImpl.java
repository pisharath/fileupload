package com.hk.fl.daoI.daoImpl;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.hk.fl.daoI.DaoI;
import com.hk.fl.domain.TempTable;
import com.hk.fl.domain.User;
import com.hk.fl.util.TempMapper;
import com.hk.fl.util.UserMapper;

/**
 * 
 * @author Vimal Bhaskar 02/19/2020
 *
 */
@Repository
public class DaoImpl implements DaoI {

	private Logger logger = LoggerFactory.getLogger(DaoImpl.class);
	public String File_Name="";
	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
    public DataSource dataSource;
	@Override
	public User authUser(User user) throws Exception {
		String query = "SELECT * FROM users where id = ? and password = ?";
		User result = (User) jdbcTemplate.queryForObject(query, new Object[] { user.getUserId(), user.getPassword() },
				new UserMapper());
		return result;
	}

	@Override
	public List<TempTable> uploadFile(InputStream file1) throws Exception {
		
		FileInputStream file = (FileInputStream) file1;
		File_Name=file.toString();
		// Create Workbook instance holding reference to .xlsx file
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		// Get first/desired sheet from the workbook
		XSSFSheet sheet = workbook.getSheetAt(0);
		ArrayList<TempTable> tempList = new ArrayList<>();
		// I've Header and I'm ignoring header for that I've +1 in loop
		for (int i = sheet.getFirstRowNum() + 1; i <= sheet.getLastRowNum(); i++) {
			TempTable e = new TempTable();
			Row ro = sheet.getRow(i);
			for (int j = ro.getFirstCellNum(); j <= ro.getLastCellNum(); j++) {
				Cell ce = ro.getCell(j);
				if (j == 0) {
					// If you have Header in text It'll throw exception because it won't get
					e.setId(String.valueOf(ce.getNumericCellValue()));
				}
				if (j == 1) {
					e.setSSN(ce.getStringCellValue());
				}
				if (j == 2) {
					e.setSYS_WD_OPT_CD(ce.getStringCellValue());
				}
				if (j == 3) {
					e.setReq_Amt(ce.getNumericCellValue());
				}
				if (j == 4) {
					e.setFREQ_CD(ce.getStringCellValue());
				}
				if (j == 5) {
					e.setSTART_DT(ce.getDateCellValue());
				}
				if (j == 6) {
					e.setSTOP_DT(ce.getDateCellValue());
				}
				if (j == 7) {
					e.setPREV_PAYMENT_DT(ce.getDateCellValue());
				}
				if (j == 8) {
					e.setNEXT_PAYMENT_DT(ce.getDateCellValue());
				}
				if (j == 9) {
					e.setFED_TAX_PCT(ce.getNumericCellValue());
				}
				if (j == 10) {
					e.setSTATE_TAX_PCT(ce.getNumericCellValue());
				}
				if (j == 11) {
					e.setLOCAL_TAX_PCT(ce.getNumericCellValue());
				}
			if (j == 12) {
				e.setEFT_ACCT_NO(ce.getStringCellValue());
			}
			if (j == 13) {
				e.setEFT_ACCT_TYP_C(ce.getStringCellValue());
			}
			if (j == 14) {
				e.setABA_ROUTING_NO(ce.getStringCellValue());
			}
			if (j == 15) {
				e.setDIRECT_DEPOSIT_C(ce.getStringCellValue());
			}				
			}
			tempList.add(e);
		}
		for (TempTable emp 	: tempList) {
			System.out.println("ID:" + emp.getId() + " SSN:" + emp.getSSN());
		}
		int[][] counts = batchInsert(tempList, 200);
		List<TempTable> result = jdbcTemplate.query("SELECT * FROM temp_table", new TempMapper());
		
		SimpleJdbcCall actor = new SimpleJdbcCall(dataSource).withProcedureName("PPAY_DADP_FILE");
		SqlParameterSource params = new MapSqlParameterSource();
		((MapSqlParameterSource) params).addValue("File_Name", "value1");
		Map<String, Object> outParams = actor.execute(params);
		String error = (String) outParams.get("out_exception");
		System.out.println("ERROR = "+(String) outParams.get("out_exception"));
		
		return result;
	}

	public int[][] batchInsert(List<TempTable> tempTable, int batchSize) {

		int[][] updateCounts = jdbcTemplate.batchUpdate("insert into temp_table (id, name, address,req_amt,Freq_cd,Start_DT,STOP_DT,"
				+ "PREV_PAYMENT_DT,NEXT_PAYMENT_DT, FED_TAX_PCT,STATE_TAX_PCT, LOCAL_TAX_PCT,EFT_ACCT_NO,EFT_ACCT_TYP_C,ABA_ROUTING_NO,DIRECT_DEPOSIT_C,File_Name) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				tempTable, batchSize, new ParameterizedPreparedStatementSetter<TempTable>() {
					public void setValues(PreparedStatement ps, TempTable argument) throws SQLException {
						ps.setString(1, argument.getId());
						ps.setString(2, argument.getSSN());
						ps.setString(3, argument.getSYS_WD_OPT_CD());
						ps.setDouble(4,argument.getReq_Amt());
						ps.setString(5, argument.getFREQ_CD());
						ps.setDate(6, new java.sql.Date(argument.getSTART_DT().getDate()));
						ps.setDate(7, new java.sql.Date(argument.getSTOP_DT().getDate()));
						ps.setDate(8, new java.sql.Date(argument.getPREV_PAYMENT_DT().getDate()));
						ps.setDate(9, new java.sql.Date(argument.getNEXT_PAYMENT_DT().getDate()));	
						ps.setDouble(10, argument.getFED_TAX_PCT());
						ps.setDouble(11, argument.getSTATE_TAX_PCT());
						ps.setDouble(12, argument.getLOCAL_TAX_PCT());
						
						ps.setString(13, argument.getEFT_ACCT_NO());
						ps.setString(14, argument.getEFT_ACCT_TYP_C());
						ps.setString(15, argument.getABA_ROUTING_NO());
						ps.setString(16, argument.getDIRECT_DEPOSIT_C());	
						ps.setString(17, File_Name);
					}
				});
		return updateCounts;

	}
	
	@Override
	public List<TempTable> getData() throws Exception {
		return jdbcTemplate.query("SELECT * FROM temp_table", new TempMapper());
	}
}
